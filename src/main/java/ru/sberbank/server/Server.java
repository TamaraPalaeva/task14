package ru.sberbank.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;

public class Server {
    private static ServerSocket server;// серверсокет
    private static BufferedReader reader;// поток чтения из сокета
    private static volatile BufferedWriter writer;// поток записи в сокет
    public static final int PORT = 9999;
    private static volatile Accounts accounts = new Accounts();
    private static String word;

    public static void main(String[] args) throws IOException {
        try {
            server = new ServerSocket(PORT);// серверсокет прослушивает порт 4004
            System.out.println("Сервер запущен!");// хорошо бы серверу  объявить о своем запуске
            while (true) {
                Socket clientSocket = server.accept();
                newConnect(clientSocket);
            }
        } finally {
            disconnect();
        }

    }

    private static void newConnect(Socket clientSocket) {
        new Thread(() -> {
            try (Socket accept = clientSocket) {// accept() будет ждать пока кто-нибудь не захочет подключиться
                reader = new BufferedReader(new InputStreamReader(accept.getInputStream()));// теперь мы можем принимать сообщения
                writer = new BufferedWriter(new OutputStreamWriter(accept.getOutputStream()));// и отправлять
                if (connect()) {
                    while (true) {
                        newMessage();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private static void newMessage() {
        try {
            word = reader.readLine();// ждём пока клиент что-нибудь нам напишет
            System.out.println(word);
            String name = word.substring(0, word.indexOf(" >>"));
            for (Map.Entry account : accounts.getAccounts().entrySet()) {
                if (!account.getKey().equals(name)) {
                    send((BufferedWriter) account.getValue(), word);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void disconnect() throws IOException {
        System.out.println("Сервер закрыт!");
        server.close();
        reader.close();
        writer.close();
    }


    public static synchronized boolean connect() throws IOException {
        String login = reader.readLine();
        if (accounts.containsName(login)) {
            accounts.setAccounts(login, writer);
            System.out.println("Авторизован " + login);
            writer.write("OK\r\n");
            writer.flush();
            for (Map.Entry account : accounts.getAccounts().entrySet()) {
                if (!account.getKey().equals(login)) {
                    send((BufferedWriter) account.getValue(), "К беседе присоединился " + login);
                }
            }
            return true;
        }
        return false;
    }

    public synchronized static void send(BufferedWriter out, String msg) {

        try {
            out.write(msg + "\r\n");
            out.flush();
        } catch (IOException ignored) {
        }
    }
}