package ru.sberbank.server;

import java.io.BufferedWriter;
import java.util.*;

public class Accounts {
    private Set<String> names = new HashSet<>(Arrays.asList("антон", "анна", "вера", "иван"));
    private final Map<String, BufferedWriter> accounts = new HashMap<>();

    public Accounts() {
    }

    public BufferedWriter getSocket(String account) {
        return accounts.get(account);
    }

    public Map<String, BufferedWriter> getAccounts() {
        return accounts;
    }

    public void setAccounts(String account, BufferedWriter accept) {
        this.accounts.put(account, accept);

    }

    public boolean containsName(String name) {
        return names.contains(name);

    }

    public int getSizeConnect() {
        return accounts.size();
    }
}
