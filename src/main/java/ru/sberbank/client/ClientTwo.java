package ru.sberbank.client;

import java.io.*;
import java.net.Socket;

public class ClientTwo {

    public static final int DEFAULT_PORT = 9999;
    public static final String DEFAULT_HOST = "localhost";

    private static BufferedReader console;// нам нужен ридер читающий с консоли, иначе как мы узнаем что хочет сказать клиент?
    private static BufferedReader in;// поток чтения из сокета
    private static BufferedWriter out;// поток записи в сокет
    private static String account;
    private static String word = "";
    private static String serverWord;

    public static void main(String[] args) {
        try {
            try (Socket clientSocket = new Socket(DEFAULT_HOST, DEFAULT_PORT)) {
                console = new BufferedReader(new InputStreamReader(System.in));
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
                if (connect()) {
                    do {
                        newMessageServer();
                        newMessageClient();
                    } while (!word.equals("exit"));
                    clientSocket.close();
                }
            } finally {
                disConnect();
            }
        } catch (IOException e) {
            System.err.println(e);
        }

    }

    private static void newMessageServer() throws IOException {
        if (in.ready()) {
            serverWord = in.readLine();
            System.out.println(serverWord);
        }
    }

    private static void newMessageClient() throws IOException {
        if (console.ready()) {
            word = console.readLine();
            out.write(account + " >> " + word + "\r\n");
            out.flush();
        }
    }

    private static void disConnect() throws IOException {
        System.out.println("Клиент был закрыт...");
        in.close();
        out.close();
    }

    private static boolean connect() throws IOException {
        System.out.println("Введите логин: антон");
        //account = console.readLine();
        account = "антон";
        out.write(account + "\r\n");
        out.flush();
        serverWord = in.readLine();
        if (serverWord.equals("OK")) {
            System.out.println("Авторизация прошла успешно");
            return true;
        } else return false;
    }
}

